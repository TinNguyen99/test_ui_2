import React, {useState} from 'react';
import {View, Text, TouchableOpacity, TextInput} from 'react-native';
import Icon from 'react-native-vector-icons/dist/Ionicons';
import COLORS from '../../utils/colors';
import LinearGradient from 'react-native-linear-gradient';
import styles from './styles';

export const HeaderApp = ({isHaveSearchBar, title}) => {
  const [text, onChangeText] = useState('Tìm kiếm ...');
  return (
    <LinearGradient
      style={styles.linear}
      start={{x: 0, y: 0}}
      end={{x: 1, y: 1}}
      colors={[COLORS.linearColor1, COLORS.linearColor2, COLORS.linearColor3]}>
      <View style={styles.container}>
        <TouchableOpacity style={{position: 'absolute', left: 15}}>
          <Icon name="chevron-back-outline" size={30} color={'white'} />
        </TouchableOpacity>
        <Text style={styles.title}>{title}</Text>
      </View>
      {isHaveSearchBar ? (
        <View style={styles.rowSearchBar}>
          <View style={styles.searchBar}>
            <View style={styles.iconLeft}>
              <Icon name="search-outline" size={20} color={'white'} />
            </View>
            <View style={styles.input}>
              <TextInput onChangeText={onChangeText} value={text} />
            </View>
          </View>
          <View style={styles.filter}>
            <Icon name="funnel-outline" size={20} color={'white'} />
            <Text style={styles.txtFilter}>Lọc</Text>
          </View>
        </View>
      ) : (
        <></>
      )}
    </LinearGradient>
  );
};
