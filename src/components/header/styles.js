import {StyleSheet} from 'react-native';
import COLORS from '../../utils/colors';
import {responsiveWidth, responsiveHeight} from '../../utils/metrics';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    fontWeight: '400',
    justifyContent: 'center',
    alignItems: 'center',
  },
  linear: {
    flex: 1,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
  },
  title: {
    alignSelf: 'center',
    color: 'white',
    fontSize: 18,
    fontWeight: '400',
  },
  rowSearchBar: {
    height: responsiveHeight(50),
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: responsiveHeight(20),
    marginHorizontal: responsiveWidth(20),
  },
  searchBar: {
    flexDirection: 'row',
    height: responsiveHeight(50),
    alignItems: 'center',
  },
  filter: {
    flexDirection: 'row',
    height: responsiveHeight(50),
    width: '20%',
    borderRadius: 10,
    backgroundColor: '#e3e2e2',
    justifyContent: 'center',
    alignItems: 'center',
    opacity: 0.4,
    paddingHorizontal: responsiveWidth(10),
  },
  iconLeft: {
    height: responsiveHeight(50),
    justifyContent: 'center',
    paddingHorizontal: responsiveWidth(10),
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
    backgroundColor: '#e3e2e2',
    opacity: 0.4,
  },
  input: {
    height: responsiveHeight(50),
    width: '70%',
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
    backgroundColor: '#e3e2e2',
    opacity: 0.4,
  },
  txtFilter: {color: 'white', fontWeight: 'bold', paddingStart: 10},
});

export default styles;
