export const mockData = [
  {
    id: 1,
    name: 'Nguyễn Văn B',
    address: 'Phường 3, Quận 5, TP HCM',
    shareBy: 'Lê Lang Leo',
    status: 'String Status',
    image:
      'https://upload.wikimedia.org/wikipedia/commons/thumb/1/12/User_icon_2.svg/1024px-User_icon_2.svg.png',
  },
  {
    id: 2,
    name: 'Nguyễn Văn C',
    address: 'Phường 3, Quận 5, TP HCM',
    shareBy: 'Lê Lang Leo',
    status: 'String Status',
    image:
      'https://upload.wikimedia.org/wikipedia/commons/thumb/1/12/User_icon_2.svg/1024px-User_icon_2.svg.png',
  },
  {
    id: 3,
    name: 'Nguyễn Văn C',
    address: 'Phường 3, Quận 5, TP HCM',
    shareBy: 'Lê Lang Leo',
    status: 'String Status',
    image:
      'https://upload.wikimedia.org/wikipedia/commons/thumb/1/12/User_icon_2.svg/1024px-User_icon_2.svg.png',
  },
  {
    id: 4,
    name: 'Nguyễn Văn C',
    address: 'Phường 3, Quận 5, TP HCM',
    shareBy: 'Lê Lang Leo',
    status: 'String Status',
    image:
      'https://upload.wikimedia.org/wikipedia/commons/thumb/1/12/User_icon_2.svg/1024px-User_icon_2.svg.png',
  },
  {
    id: 5,
    name: 'Nguyễn Văn C',
    address: 'Phường 3, Quận 5, TP HCM',
    shareBy: 'Lê Lang Leo',
    status: 'String Status',
    image:
      'https://upload.wikimedia.org/wikipedia/commons/thumb/1/12/User_icon_2.svg/1024px-User_icon_2.svg.png',
  },
  {
    id: 6,
    name: 'Nguyễn Văn C',
    address: 'Phường 3, Quận 5, TP HCM',
    shareBy: 'Lê Lang Leo',
    status: 'String Status',
    image:
      'https://upload.wikimedia.org/wikipedia/commons/thumb/1/12/User_icon_2.svg/1024px-User_icon_2.svg.png',
  },
  {
    id: 7,
    name: 'Nguyễn Văn C',
    address: 'Phường 3, Quận 5, TP HCM',
    shareBy: 'Lê Lang Leo',
    status: 'String Status',
    image:
      'https://upload.wikimedia.org/wikipedia/commons/thumb/1/12/User_icon_2.svg/1024px-User_icon_2.svg.png',
  },
];
