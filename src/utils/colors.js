export default {
  linearColor1: '#9045e8',
  linearColor2: '#3368c2',
  linearColor3: '#3eabe8',
  darkBlue: '#3e68de',
  darkGreen: '#407f65',
  lightGreen: '#eaf6ea',
  backgroundColor: '#e5e5e5',
};
