import {Dimensions} from 'react-native';

const {width, height} = Dimensions.get('window');

export const responsiveWidth = value => (value / 414) * width;

export const responsiveHeight = value => (value / 736) * height;
