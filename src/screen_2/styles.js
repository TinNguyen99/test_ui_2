import {StyleSheet} from 'react-native';
import COLORS from '../utils/colors';
import {responsiveWidth, responsiveHeight} from '../utils/metrics';

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    backgroundColor: COLORS.darkBackground,
  },
});

export default styles;
