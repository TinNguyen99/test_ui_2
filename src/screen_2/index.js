import React from 'react';
import {View} from 'react-native';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import ReservationlPage from '../screen_2/reservation_page';
import DepositPage from '../screen_2/deposit_page';
import SuccessPage from '../screen_2/success_page';
import CancelPage from '../screen_2/cancel_page';

import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {HeaderApp} from '../components/header/header';
import COLORS from '../utils/colors';

const Tab = createMaterialTopTabNavigator();
const Stack = createStackNavigator();

const Screen2 = () => (
  <NavigationContainer>
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen
        name="Main"
        component={MyTab}
        options={{
          animationEnabled: false,
        }}
      />
    </Stack.Navigator>
  </NavigationContainer>
);

function MyTab() {
  return (
    <Tab.Navigator
      tabBarOptions={{
        activeTintColor: COLORS.linearColor3,
        inactiveTintColor: 'grey',
        labelStyle: {fontSize: 10, fontWeight: 'bold'},
      }}>
      <Tab.Screen name="Giữ chỗ" component={ReservationlPage} />
      <Tab.Screen name="Đặt cọc" component={DepositPage} />
      <Tab.Screen name="Thành công" component={SuccessPage} />
      <Tab.Screen name="Đã hủy" component={CancelPage} />
    </Tab.Navigator>
  );
}

const MainScreen = () => {
  return (
    <>
      <View style={{height: 160, width: '100%'}}>
        {HeaderApp({isHaveSearchBar: true, title: 'Danh sách Booking'})}
      </View>
      <View style={{flex: 1, width: '100%'}}>{Screen2()}</View>
    </>
  );
};

export default MainScreen;
