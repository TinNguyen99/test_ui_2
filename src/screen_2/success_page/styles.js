import {StyleSheet} from 'react-native';
import COLORS from '../../utils/colors';
import {responsiveWidth, responsiveHeight} from '../../utils/metrics';

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    backgroundColor: COLORS.darkBackground,
  },
  card: {
    borderRadius: 20,
    width: '90%',
    alignSelf: 'center',
    marginTop: responsiveHeight(15),
    flex: 1,
    backgroundColor: 'white',
  },
  topCard: {
    flex: 1,
  },
  titleCard: {
    marginVertical: responsiveHeight(10),
    marginStart: responsiveWidth(10),
    position: 'absolute',
    bottom: 40,
    left: 20,
  },
  imageBackground: {
    flex: 1,
    height: responsiveHeight(220),
  },
  txtTitle: {
    fontWeight: 'bold',
    color: 'white',
    fontSize: 14,
  },
  subTitleCard: {
    flexDirection: 'row',
    marginVertical: responsiveHeight(10),
    marginStart: responsiveWidth(10),
    position: 'absolute',
    bottom: 10,
    left: responsiveWidth(20),
  },
  groupSubTitle: {
    flexDirection: 'row',
    marginEnd: responsiveWidth(30),
  },
  txtSubTitle: {
    color: 'white',
    fontSize: 10,
    paddingStart: responsiveWidth(8),
  },
  statusCard: {
    position: 'absolute',
    right: 0,
    top: 0,
    backgroundColor: '#3eb161',
    borderBottomLeftRadius: 20,
    borderTopRightRadius: 15,
    justifyContent: 'center',
    alignItems: 'center',
    height: responsiveHeight(30),
    width: responsiveWidth(120),
  },
  txtStatus: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 14,
  },
  bodyCard: {
    paddingVertical: responsiveHeight(20),
    paddingHorizontal: responsiveWidth(20),
    justifyContent: 'center',
    alignItems: 'center',
  },
  rowBody: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  titleBody: {
    fontSize: 14,
    color: '#b8b5b5',
    paddingVertical: responsiveHeight(5),
  },
  contentBody: {
    fontSize: 14,
    fontWeight: '700',
    color: 'black',
    paddingVertical: responsiveHeight(5),
  },
  lineGrey: {
    height: 1,
    width: '100%',
    backgroundColor: 'grey',
    marginBottom: responsiveHeight(10),
  },
  bottomCard: {
    paddingBottom: responsiveHeight(10),
  },
});

export default styles;
