import React, {useState} from 'react';
<View style={styles.topCard} />;
import {
  View,
  Text,
  FlatList,
  TouchableOpacity,
  ImageBackground,
} from 'react-native';
import {mockData} from '../../constants';
import styles from './styles';
import Icon from 'react-native-vector-icons/dist/Ionicons';

const SuccessPage = () => {
  const renderItem = ({item}) => {
    return (
      <View style={styles.card}>
        <View style={styles.topCard}>
          <ImageBackground
            source={{
              uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRbs1azERWx8RawApKCB21IqHS__wr-OWnzdw&usqp=CAU',
            }}
            imageStyle={{borderTopLeftRadius: 15, borderTopRightRadius: 15}}
            resizeMode="stretch"
            style={styles.imageBackground}>
            <View style={styles.titleCard}>
              <Text style={styles.txtTitle}>METRO STAR</Text>
            </View>
            <View style={styles.subTitleCard}>
              <View style={styles.groupSubTitle}>
                <Icon name="bed-outline" size={15} color={'white'} />
                <Text style={styles.txtSubTitle}>1200</Text>
              </View>
              <View style={styles.groupSubTitle}>
                <Icon name="bed-outline" size={15} color={'white'} />
                <Text style={styles.txtSubTitle}>2</Text>
              </View>
              <View style={styles.groupSubTitle}>
                <Icon name="bed-outline" size={15} color={'white'} />
                <Text style={styles.txtSubTitle}>99</Text>
              </View>
              <View style={styles.groupSubTitle}>
                <Icon name="bed-outline" size={15} color={'white'} />
                <Text style={styles.txtSubTitle}>50</Text>
              </View>
              <View style={styles.groupSubTitle}>
                <Icon name="bed-outline" size={15} color={'white'} />
                <Text style={styles.txtSubTitle}>18</Text>
              </View>
            </View>
            <View style={styles.statusCard}>
              <Text style={styles.txtStatus}>Thành công</Text>
            </View>
          </ImageBackground>
        </View>
        <View style={styles.bodyCard}>
          <View style={styles.rowBody}>
            <Text style={styles.titleBody}>Khách hàng:</Text>
            <Text style={styles.contentBody}>{item.name.toUpperCase()}</Text>
          </View>
          <View style={styles.rowBody}>
            <Text style={styles.titleBody}>Mã hợp đồng:</Text>
            <Text style={styles.contentBody}>YTNZIINFFX</Text>
          </View>
          <View style={styles.rowBody}>
            <Text style={styles.titleBody}>Ngày đặt:</Text>
            <Text style={styles.contentBody}>12/12/2021</Text>
          </View>
        </View>

        <View style={styles.lineGrey} />

        <View style={styles.bottomCard}>
          <View style={[styles.rowBody, {paddingHorizontal: 20}]}>
            <Text style={styles.titleBody}>Số tiền đã đóng:</Text>
            <Text style={[styles.contentBody, {color: '#fbd27a'}]}>
              50.000.000 VND
            </Text>
          </View>
        </View>
      </View>
    );
  };

  return (
    <View>
      <View style={{width: '100%'}}>
        <FlatList
          data={mockData}
          renderItem={renderItem}
          keyExtractor={item => item.id}
        />
      </View>
    </View>
  );
};

export default SuccessPage;
