import React from 'react';
import {View, Text} from 'react-native';

const DepositPage = () => {
  return (
    <View
      style={{
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: '100%',
      }}>
      <Text>Deposit Page</Text>
    </View>
  );
};

export default DepositPage;
