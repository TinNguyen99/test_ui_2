import React, {useState} from 'react';
import {View, Text, FlatList, TouchableOpacity, Image} from 'react-native';
import styles from './styles';
import {HeaderApp} from '../components/header/header';
import {mockData} from '../constants';
import Icon from 'react-native-vector-icons/dist/Ionicons';

const Screen1 = () => {
  const renderItem = ({item}) => {
    return (
      <View style={styles.itemContainer}>
        <View style={styles.rowContainer}>
          <Image
            style={styles.avatar}
            source={{
              uri: item.image,
            }}
          />
          <View>
            <Text>{item.name}</Text>
            <Text>Chia sẻ bởi {item.shareBy}</Text>
            <Text>{item.address}</Text>
          </View>

          <View style={styles.addButton}>
            <TouchableOpacity
              style={{flexDirection: 'row'}}
              onPress={() => {
                console.log('ADD');
              }}>
              <Icon name="add-outline" size={20} color={'white'} />
              <Text style={styles.txtAdd}>Thêm</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.lineGrey} />

        <View style={styles.rowItem}>
          <Text style={styles.status}>Status: </Text>
          <Text numberOfLines={3} ellipsizeMode="tail">
            {item.status}
          </Text>
        </View>
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <View style={{height: 120, width: '100%'}}>
        {HeaderApp({isHaveSearchBar: false, title: 'Danh sách Leads tài năng'})}
      </View>
      <View style={{width: '100%'}}>
        <FlatList
          data={mockData}
          renderItem={renderItem}
          keyExtractor={item => item.id}
        />
      </View>
    </View>
  );
};

export default Screen1;
