import {StyleSheet} from 'react-native';
import COLORS from '../utils/colors';
import {responsiveWidth, responsiveHeight} from '../utils/metrics';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    height: '100%',
    backgroundColor: COLORS.backgroundColor,
  },
  avatar: {
    width: responsiveWidth(50),
    height: responsiveHeight(50),
    borderRadius: 100,
    backgroundColor: COLORS.lightGreen,
  },
  addButton: {
    width: responsiveWidth(90),
    height: responsiveHeight(35),
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: COLORS.darkBlue,
  },
  txtAdd: {
    color: 'white',
    fontSize: 14,
    fontWeight: '400',
  },
  status: {
    fontWeight: 'bold',
    fontStyle: 'italic',
    fontSize: 16,
    color: 'black',
    paddingStart: responsiveWidth(10),
  },
  lineGrey: {
    width: '90%',
    height: 1,
    backgroundColor: 'grey',
    alignSelf: 'center',
  },
  rowItem: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: responsiveHeight(10),
    paddingHorizontal: responsiveWidth(10),
  },
  rowContainer: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: responsiveHeight(10),
    paddingHorizontal: responsiveWidth(10),
  },
  itemContainer: {
    marginTop: responsiveHeight(10),
    backgroundColor: 'white',
  },
});

export default styles;
